# -*- coding: utf-8 -*-

import librosa
import os
import glob
import numpy as np
import json
import random
import time

def main():
 	for_save_list , for_error_list= input_musics()

 	with open('error_list.json','w')as f:
 		json.dump(for_error_list, f, sort_keys=True, indent=4)

 	with open('song_features.json','w')as f:
 		json.dump(for_save_list, f, sort_keys=True, indent=4)

def input_musics():
	files = glob.glob(os.path.join('/Users/kosukefukui/Qosmo/WASABEAT/music/*.mp3'))
	
	mel_spectrum_list = []
	error_list = []
	n_files = len(files)

	print "------lodaging data------"
	
	i = 0
	time_clock = 0


	for mp3_song in files:

		start = time.time()

		#ケプストラム変換 + エラー処理
		try:
			y, sr = librosa.load(mp3_song, offset = 30, duration = 13.9)
			for_append = librosa.feature.melspectrogram(y=y, sr=sr, n_mels =128)	
			for_append = for_append.T
		
		except IndexError:
			print "index error occurred!"
			print mp3_song
			error_list.append(mp3_song)
			time_clock = time_clock + (time.time() - start)
			i = i + 1

			if i % 100 == 0:
				run_num = (n_files - i) / 100
				elapsed_time = time_clock
				total_time = (run_num * elapsed_time) / 60
				last_time = int(total_time)
				time_clock = 0

				print i
				print "Project finished in %s mins " % last_time
		else:
			if for_append.shape[0] == 599:
				for_append = for_append.tolist()
				mel_spectrum_list.append(for_append)

			time_clock = time_clock + (time.time() - start)

			i = i + 1

			#残り時間確認		
			if i % 100 == 0:
				run_num = (n_files - i) / 100
				elapsed_time = time_clock
				total_time = (run_num * elapsed_time) / 60
				last_time = int(total_time)
				time_clock = 0

				print i
				print "Project finished in %s mins " % last_time

	return (mel_spectrum_list , error_list)


if __name__ == '__main__':
	
	main()
