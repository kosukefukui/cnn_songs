# -*- coding: utf-8 -*-
import json
import subprocess
import urllib
import os

os.chdir("./json")

for t in range(1,50):
	cmd = 'curl -XGET "http://api.wasabeat.com/v1/tracks/search?genre_id=600,601,700,701,900,901,902,903,905,907,909,1100,1101,1102,1300,1301,1302&count=100&page=%s"' % t

	print "----loading data----"
	data = subprocess.check_output(cmd, shell= True)
	data2 = json.loads(data)

	with open('music_url%s.json' % t,'w')as f: 
 		json.dump(data2, f, sort_keys=True, indent = 4)