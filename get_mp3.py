# -*- coding: utf-8 -*-
import json
import subprocess
import urllib
import os
import glob
import time

print "----download mp3 samples…----"

filenames = glob.glob(os.path.join('/Users/kosukefukui/Qosmo/WASABEAT/json/*.json'))

num = 0
total_num = 100 * len(filenames)

for file_json in filenames:
	start = time.time()
	data = json.load(open(file_json))
	n_tracks = len(data["tracks"])

	for i in range(n_tracks):
		track_id = data["tracks"][i]["id"]
		sample_url = data["tracks"][i]["urls"]["sample"]
		urllib.urlretrieve(sample_url,"/Users/kosukefukui/Qosmo/WASABEAT/music/%s.mp3"% track_id)

	elapsed_time = time.time() - start

	num = num + 100
	run_num = (total_num - num) / 100
	min_time = (run_num * elapsed_time) / 60
	last_time = int(min_time)

	print num
	print "Project finished in %s mins " % last_time