# -*- coding: utf-8 -*-
import json
import subprocess
import urllib
import os
import glob
import time
import numpy as np

#出力数
NUM_CLASSES = 5
#データのframe数
IMAGE_SIZE = 599
#データの全形
IMAGE_PIXELS = IMAGE_SIZE*1*128
train_num = 70
data_limit = 100

flatten_data = []
flatten_label = []

# データの整形
filenames = glob.glob(os.path.join('/Users/kosukefukui/Qosmo/WASABEAT/song_features/*.json'))
filenames = filenames[0:data_limit]
print "----loading data---"
for file_path in filenames:
	data = json.load(open(file_path))
	data = np.array(data)

	for_flat = np.array(data)
	assert for_flat.flatten().shape == (IMAGE_PIXELS,)
	flatten_data.append(for_flat.flatten().tolist())

# ラベルの整形
f2 = open("id_information.txt")
print "---loading labels----"

for line in f2:
	line = line.rstrip()
	l = line.split(",")
	tmp = np.zeros(NUM_CLASSES)
	tmp[int(l[4])] = 1
	flatten_label.append(tmp)

flatten_label = flatten_label[0:data_limit]

print "データ数 %s" % len(flatten_data)
print "ラベルデータ数 %s" % len(flatten_label)

#訓練データ
train_image = np.asarray(flatten_data[0:train_num], dtype=np.float32)
train_label = np.asarray(flatten_label[0:train_num],dtype=np.float32)

print "訓練データ数 %s" % len(train_image)

#テストデータ
test_image = np.asarray(flatten_data[train_num:data_limit], dtype=np.float32)
test_label = np.asarray(flatten_label[train_num:data_limit],dtype=np.float32)

print "テストデータ数 %s" % len(test_image)